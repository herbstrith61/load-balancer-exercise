# Setup

All you need is to have node.js installed on your machine.

You can get a node binary for your system on:

https://nodejs.org/en/download/

# How to run

There are 3 "test" files where you can use to execute and watch all the functionalities of the load balancer being executed in the terminal.

#### Basic Functionalities test
To run the basic functionalities "test" cases you can do the following:

$ node ./tests/basicFunctionalities.js

Where you can see the execution of the "Random" Get, "Round Robin" Get and of the add and remove provider methods, as well as tests to the limit of number of providers

#### Health check test

To run the health check "test" you can do the following:

$ node ./tests/healthCheck.js

The script is going to keep executing and you can see one provider becoming healthy and unhealthy (flaky), while get requests are made by a poller.
The response from the flaky provider get is "Flaky-provider-id-response" and you should see it intermitently.

#### Maximum load test

To run the maximum load "test" you can do the following:

$ node ./tests/maxLoad.js

The script is going to keep executing and you can see one provider becoming healthy and unhealthy (flaky), while a batch of 5 requests is made every 1 second. You will see the array of responses from every batch of requests made and you should intermitently see a "500" response in the array.
