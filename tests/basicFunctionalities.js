/*
  tests/visualization for steps 1 to 5
*/
const Provider = require('../src/provider');
const LoadBalancer = require('../src/loadBalancer');

(async () => {

  providers = [
    new Provider(5),
    new Provider(5),
    new Provider(5)
  ];


  const randomLoadBalancer = new LoadBalancer(providers, "random");

  const RRLoadBalancer = new LoadBalancer(providers, "roundRobin");

  console.log("------ test random algorithm (3 providers) ------");
  console.log(await randomLoadBalancer.Get());
  console.log(await randomLoadBalancer.Get());
  console.log(await randomLoadBalancer.Get());
  console.log(await randomLoadBalancer.Get());


  console.log("------ test round robin algorithm (3 providers) ------");
  console.log(await RRLoadBalancer.Get());
  console.log(await RRLoadBalancer.Get());
  console.log(await RRLoadBalancer.Get());
  console.log(await RRLoadBalancer.Get());
  console.log(await RRLoadBalancer.Get());
  console.log(await RRLoadBalancer.Get());

  console.log("------ test add provider ------");
  const newProvider = new Provider(1);
  console.log(RRLoadBalancer.providers);
  RRLoadBalancer.AddProvider(newProvider);
  console.log(RRLoadBalancer.providers);

  console.log("------ test remove provider ------");
  console.log(RRLoadBalancer.providers);
  RRLoadBalancer.RemoveProvider(newProvider.uid);
  console.log(RRLoadBalancer.providers);
  RRLoadBalancer.RemoveProvider("gibberish");


  console.log("------ test Maximum provider limit ------");
  const elevenProviders = [
    new Provider(5),
    new Provider(5),
    new Provider(5),
    new Provider(5),
    new Provider(5),
    new Provider(5),
    new Provider(5),
    new Provider(5),
    new Provider(5),
    new Provider(5),
    new Provider(5)
  ];
  try {
    let newLoadBalancer = new LoadBalancer(elevenProviders);
  } catch (error) {
    console.log(error);
  }

  console.log("------ test addProvider provider limit ------");

  newLoadBalancer = new LoadBalancer([]);
  elevenProviders.forEach((provider) => {
    try {
      newLoadBalancer.AddProvider(provider);
    } catch (error) {
      console.log(error);
    }
  })
})()
