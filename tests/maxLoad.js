/*
  tests/visualization for steps 8
*/const Provider = require('../src/provider');
const LoadBalancer = require('../src/loadBalancer');

// 60% chance of failure
const flakyCheckMethod = () => (Math.random() < 0.40);
const sleep = (ms) => {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}
const delayedGetMethod = async () => {
  await sleep(3000);
  return "Provider with a delayed response";
};

const parallelPollerTest = async (loadBalancer, time) => {
  await setTimeout(async () => {
    const promises = [
      loadBalancer.Get(),
      loadBalancer.Get(),
      loadBalancer.Get(),
      loadBalancer.Get(),
      loadBalancer.Get()
    ];
    const responses = await Promise.all(promises);
    console.log("----- Parallel execution responses ------");
    responses.forEach((response) => console.log(response));

    parallelPollerTest(loadBalancer, time);
   }, time);
};

(async () => {  
  const slowProviders = [
    new Provider(1, undefined, delayedGetMethod),
    new Provider(1, undefined, delayedGetMethod),
    new Provider(1, undefined, delayedGetMethod),
    new Provider(1, undefined, delayedGetMethod),
    new Provider(1, flakyCheckMethod, delayedGetMethod)
  ];

  console.log("------ Round Robin Load Balancer with 4 healthy providers and 1 flaky provider (providers take 3 seconds to respond) ------");
  const limitTestLoadBalancer = new LoadBalancer(slowProviders, "roundRobin");
  console.log("------ Start health check process every 1 second ------");
  limitTestLoadBalancer.HealthCheck(1000);
  console.log("------ Poll the load balancer with 5 requests every 1 second ------");
  parallelPollerTest(limitTestLoadBalancer, 1000);
})()
