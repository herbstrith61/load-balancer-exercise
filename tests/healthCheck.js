/*
  tests/visualization for steps 6 and 7
*/
const Provider = require('../src/provider');
const LoadBalancer = require('../src/loadBalancer');

const brokenCheckMethod = () => (false);

// 60% chance of failure
const flakyCheckMethod = () => (Math.random() < 0.40);

const syncPollerTest = async (loadBalancer, time) => {
  await setTimeout(async () => {
    console.log(await loadBalancer.Get());
    syncPollerTest(loadBalancer, time);
   }, time);
};

(async () => {
  providers = [
    new Provider(5),
    new Provider(5),
    new Provider(5),
    new Provider(5, brokenCheckMethod),
    new Provider(5, flakyCheckMethod, () => "Flaky-provider-id-response")
  ];

  console.log("------ Round Robin Load Balancer with 4 healthy providers, 1 unhealthy provider and 1 flaky provider ------");
  const RRLoadBalancer = new LoadBalancer(providers, "roundRobin");

  console.log("------ Start health check process (every 1 second) ------");
  RRLoadBalancer.HealthCheck(1000);
  console.log("------ test health-check by polling the loadbalancer continuously ------");
  syncPollerTest(RRLoadBalancer, 300);
})()
