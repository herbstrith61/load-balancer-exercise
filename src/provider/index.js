const randomString = require("../helpers/randomString");

class Provider {
  constructor(maxLoad, checkOverride, getOverride) {
    this.uid = randomString();
    this.maxLoad = maxLoad;
    this.checkOverride = checkOverride;
    this.getOverride = getOverride
  }

  async get () {
    if (this.getOverride) {
      return await this.getOverride()
    }
    return this.uid;
  }

  async check () {
    if (this.checkOverride) {
      return await this.checkOverride();
    }
    return true;
  }
}

module.exports = Provider;
