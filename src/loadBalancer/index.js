const ProviderInstance = require("./helpers/providerInstance")

class LoadBalancer {
  constructor(providers, algorithm) {
    if (providers.length > 10) {
      throw new Error("Too many providers")
    }

    this.providers = providers.map((provider) => (new ProviderInstance(provider)));
    this.maxLoad = 0; 
    this.healthyProviders = [];
    this.setHealthyList();
    this.algorithm = algorithm;
    this.currentLoad = 0;
    this.roundRobinCount = 0;
  }

  async Get () {
    if ( (this.currentLoad + 1) > this.maxLoad) {
      console.log("Max load exceeded");
      return "500";
    }
    try {
      this.currentLoad++;
      switch(this.algorithm) {
        case "roundRobin":
          return await this.getRoundRobin();
        case "random":
        default:
          return await this.getRandom();
      }
    } finally {
      this.currentLoad--;
    }
  }

  RemoveProvider(id) {
    const indexToRemove = this.providers.findIndex((provider) => (
      provider.instance.get() === id
    ));
    if (indexToRemove === -1) {
      console.log(`Could not find provider with id: ${id}`);
      return;
    }
    this.providers.splice(indexToRemove, 1);
    this.setHealthyList();
  }

  AddProvider(provider) {
    if (this.providers.length >= 10) {
      throw new Error("Too many providers")
    }
    this.providers.push(new ProviderInstance(provider));
    this.setHealthyList();
  }

  HealthCheck(timeMs) {
    setTimeout(async () => {
      const promises = this.providers.map(async (provider, index) => {
        const healthCheckResponse = await provider.instance.check();
        if (healthCheckResponse === true) {
          provider.IncrementHealthCheckCount();
          if (provider.IsHealthy() === false && provider.GetHealthCheckCount() >= 2) {
            console.log(`Added provider ${await provider.id} back to pool`)
            provider.SetHealthy();
            this.setHealthyList();
          }
        } else {
          if (provider.IsHealthy()) {
            provider.SetUnhealthy();
            this.setHealthyList();
            console.log(`Removed unhealthy provider ${await provider.id}`);
          }
        }
      });
      await Promise.all(promises);
      this.HealthCheck(timeMs)
    }, timeMs);
  }

  //private methods if JS allowed that
  async getRandom() {
    const healthyProviderIndex = Math.floor(Math.random() * (this.healthyProviders.length));
    const providerIndex = this.healthyProviders[healthyProviderIndex];
    const provider = this.providers[providerIndex];
    return await provider.instance.get()
  }

  async getRoundRobin() {
    this.roundRobinCount = this.roundRobinCount < this.healthyProviders.length - 1 ? this.roundRobinCount + 1 : 0;
    const providerIndex = this.healthyProviders[this.roundRobinCount];
    const provider = this.providers[providerIndex];
    return await provider.instance.get()
  }

  setHealthyList() {
    this.healthyProviders = this.providers.reduce((acc, provider, index) => {
      if (provider.IsHealthy()) {
        acc.push(index);
      }
      return acc;
    }, []);
    this.setMaxLoad();
  }

  setMaxLoad() {
    this.maxLoad = this.providers.reduce((acc, provider) => {
      if (provider.IsHealthy()) {
        return acc + provider.instance.maxLoad;
      }
      return acc;
    }, 0);
  }
}

module.exports = LoadBalancer;
