const randomString = require("../../helpers/randomString")

class InstanceProvider {
  constructor(instance) {
    this.id = randomString();
    this.instance = instance;
    this.healthy = true;
    this.healthCheckCount = 2;
  }

  IsHealthy () {
    return this.healthy;
  }

  GetHealthCheckCount () {
    return this.healthCheckCount;
  }

  IncrementHealthCheckCount () {
    this.healthCheckCount= Math.max(this.healthCheckCount + 1, 2);
  }

  ResetHealthCheckCount () {
    this.healthCheckCount = 0;
  }

  SetHealthy() {
    this.healthy = true;
  }

  SetUnhealthy() {
    this.healthy = false;
  }
}

module.exports = InstanceProvider;
